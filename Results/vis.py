import os
import json
import pandas as pd
root = "./"
savedir = "/"
sac_colors = ["#01eb00", "#0038eb", "#eb7400", "#a365f6", "#d39292", "#000000"]
a2c_colors = ["#000000", "#f665c3", "#65f6f3", "#3b8c4a", "#9e9e9e", "#01eb00"]
for path in os.listdir(root):
  if "Experiment" in path:
    if "4x" in path:
      povertyline = 120
    elif "200x" in path:
      povertyline = 6000
    elif "Lower" in path:
      povertyline = 24
    else:
      povertyline = 30
    path = root + path + "/"
    for number in os.listdir(path):
      import matplotlib.pyplot as plt 
      plt.style.reload_library()
      subpath = path + number + "/"
      savedir = subpath

      plt.rcParams.update({'font.size': 30})
      fig= plt.figure(figsize=(50,25))
      ax = fig.gca()
      
      a2c_count = 1
      sac_count = 1

      for file in os.listdir(subpath):
        if ".ipynb_checkpoints" not in file:
          
          f = open(subpath + file, "r")
          to_read = f.readline().replace("\n", "").replace("array([", "").replace(".]", "]").replace("])", "").replace("'", '"')
          df = pd.json_normalize(json.loads(to_read)[-1])
          df = df[["Reward", "Observation.gamescore"]]

          for line in f:
            n_line = line.replace("\n", "").replace("array([", "").replace(".]", "]").replace("])", "").replace("'", '"')
            df2 = pd.json_normalize(json.loads(n_line)[-1])
            df2 = df2[["Reward", "Observation.gamescore"]]
            df = pd.concat([df, df2])


          if "SAC" in file:
            name = "SAC Agent " + str(sac_count)
            color = sac_colors[sac_count-1]
            sac_count += 1
          else:
            name = "A2C Agent " + str(a2c_count)
            color = a2c_colors[a2c_count-1]
            a2c_count += 1
          df[name] = df["Reward"] + df["Observation.gamescore"] 
          df = df[[name]]
          df.loc[df[name] < 0, name] += 10000000000 
          df.reset_index(inplace=True, drop=True)
          df["GameCount"] = df.index
          if "SAC" in name:
            df.GameCount += 249 #Because of random sampling in SAC
          
          
          df.plot(kind='line', x="GameCount", y=name, ax=ax, color=color)
          if "SAC" in name:
            plt.scatter(df.GameCount[0], df[name][0], marker='o', color=color, zorder=10, s=500)
          plt.scatter(df.iloc[-1].GameCount, df.iloc[-1][name], marker='x', color=color, zorder=2, s=500)
      df = pd.DataFrame({ 'GameCount' : range(1, 10100 + 1 ,1), 'PovertyLine' : povertyline})
      df.plot(kind="line", x="GameCount", y="PovertyLine", ax=ax, color='red', linewidth=10.0)
      

      leg = plt.legend(prop={'size': 30})
      for l in leg.legendHandles:
        l.set_linewidth(5.0)
      handles, labels = ax.get_legend_handles_labels()
      sac_list = []
      a2c_list = []
      for i in range(len(labels)):
        if "SAC" in labels[i]:
          sac_list.append(i)
        elif "A2C" in labels[i]:
          a2c_list.append(i)
        else:
          poverty = i
      n_labels = []
      n_handles = []
      for i in sac_list:
        n_labels.append(labels[i])
        n_handles.append(handles[i])
      for i in a2c_list:
        n_labels.append(labels[i])
        n_handles.append(handles[i])
      n_labels.append(labels[poverty])
      n_handles.append(handles[poverty])
      ax.legend(n_handles, n_labels, loc='upper center', bbox_to_anchor=(1.1, 0.4))
      ax.set_xlim(xmin=0, xmax=10100)
      plt.savefig(savedir + str(number) + '_plot.png')
